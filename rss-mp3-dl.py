import feedparser, argparse, sys, urllib2, os
from mutagen.mp3 import MP3
class RssArgs(argparse.ArgumentParser):

    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def DownloadFeed(url, dir):
    feed = feedparser.parse(url)
    if not os.path.exists(dir):
        print "%s does not exist.  Creating directory" % (dir)
        os.makedirs(dir)
    print "Downloading files to: %s" % (dir)
    for entry in feed.entries:
        file_name = entry['title']
        file_name = "".join([c for c in file_name if c.isalpha() or c.isdigit() or c==' ']).rstrip() + '.mp3'
        dl_dir = os.path.abspath(os.path.join(dir,file_name))
        u = urllib2.urlopen(entry['link'])      
        meta = u.info()
        file_size = int(meta.getheaders("Content-Length")[0])
        if os.path.isfile(dl_dir):
            local_file_size = os.path.getsize(dl_dir)
            if file_size == local_file_size:
                print file_name + " exists.  Skipping."
                continue
            else:
                print file_name + " exists, but looks incomplete.  Redownloading"
        print "Downloading: %s -- Bytes: %s" % (file_name, file_size)
        file_size_dl = 0
        block_sz = 8192
        f = open(dl_dir, 'wb')
        while True:
            buffer = u.read(block_sz)
            if not buffer:
                break
            
            file_size_dl += len(buffer)
            f.write(buffer)
            status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
            status = status + chr(8)*(len(status)+1)
            print status,
        f.close()


parser = argparse.ArgumentParser(description='Launch parameters.')
parser.add_argument('url', type=str, help='Address to RSS feed.')
parser.add_argument('--dir', '-d', type=str, help='Output directory')
if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)
args = parser.parse_args()
if args.dir is None:
    args.dir = os.path.dirname(os.path.join(os.getcwd(), "Downloads/"))
args.dir = args.dir.replace('"','')
args.dir = args.dir.replace('\'','')
if args.url != '':
    DownloadFeed(args.url, args.dir)

